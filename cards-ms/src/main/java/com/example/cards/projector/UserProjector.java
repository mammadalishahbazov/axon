package com.example.cards.projector;

import com.example.cards.repository.UserRepository;
import com.example.common.domain.User;
import com.example.common.events.UserCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;
    private final ModelMapper mapper;

    @EventHandler
    public void on(UserCreatedEvent event) {
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

}

