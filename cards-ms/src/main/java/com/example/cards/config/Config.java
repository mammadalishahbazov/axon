package com.example.cards.config;

import com.example.common.config.ModelMapperConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({ModelMapperConfig.class})
@Configuration
public class Config {
}
