package com.example.user.aggregates;

import com.example.common.commands.CreateUserCommand;
import com.example.common.events.UserCreatedEvent;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.UUID;

@Slf4j
@Data
@Aggregate
@NoArgsConstructor
public class UserAggregate {

    @AggregateIdentifier
    private UUID userId;

    private String displayName;

    private String email;

    private String password;


    @CommandHandler
    public UserAggregate(CreateUserCommand command){
        var aggregateId = UUID.randomUUID();
        var event = UserCreatedEvent.builder()
                .userId(aggregateId)
                .displayName(command.getDisplayName())
                .email(command.getEmail())
                .password(command.getPassword())
                .build();
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    protected void on(UserCreatedEvent userCreatedEvent){
        userId = userCreatedEvent.getUserId();
        displayName = userCreatedEvent.getDisplayName();
        email = userCreatedEvent.getEmail();
        password = userCreatedEvent.getPassword();
    }
}
