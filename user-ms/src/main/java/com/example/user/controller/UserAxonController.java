package com.example.user.controller;

import com.example.common.commands.CreateUserCommand;
import com.example.common.events.FindUserQuery;
import com.example.user.model.UserResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserAxonController {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    @PostMapping
    public UUID handle(@RequestBody CreateUserCommand command) {
        return commandGateway.sendAndWait(command);
    }

    @GetMapping("/{id}")
    public CompletableFuture<UserResponse> handle(@PathVariable String id ) {
        return queryGateway.query(new FindUserQuery(UUID.fromString(id)),
                ResponseTypes.instanceOf(UserResponse.class));
    }

}

