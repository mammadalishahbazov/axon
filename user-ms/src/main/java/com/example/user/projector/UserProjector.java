package com.example.user.projector;

import com.example.common.domain.User;
import com.example.common.events.FindUserQuery;
import com.example.common.events.UserCreatedEvent;
import com.example.user.model.UserResponse;
import com.example.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;
    private final ModelMapper mapper;

    @EventHandler
    public void on(UserCreatedEvent event) {
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

    @QueryHandler
    @Transactional
    public UserResponse handle(FindUserQuery query) {
        User user = repository.findById(query.getId()).orElse(null);
        return mapper.map(user, UserResponse.class);
    }


}

