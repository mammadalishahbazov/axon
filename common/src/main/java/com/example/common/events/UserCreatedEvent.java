package com.example.common.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class UserCreatedEvent {

    private UUID userId;

    private String displayName;

    private String email;

    private String password;
}

