package com.example.common.commands;

import lombok.Data;

@Data
public class CreateUserCommand {
    private String displayName;
    private String email;
    private String password;
}
