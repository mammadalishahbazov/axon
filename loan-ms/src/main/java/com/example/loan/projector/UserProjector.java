package com.example.loan.projector;

import com.example.common.domain.User;
import com.example.common.events.UserCreatedEvent;
import com.example.loan.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserProjector {

    private final UserRepository repository;

    @EventHandler
    public void on(UserCreatedEvent event) {
        var user = User.builder()
                .id(event.getUserId())
                .displayName(event.getDisplayName())
                .email(event.getEmail())
                .password(event.getPassword())
                .build();
        repository.save(user);
    }

}

