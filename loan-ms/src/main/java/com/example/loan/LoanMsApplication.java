package com.example.loan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan({"com.example","org.axonframework.eventhandling.tokenstore.jpa"})
public class LoanMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoanMsApplication.class, args);
    }

}
